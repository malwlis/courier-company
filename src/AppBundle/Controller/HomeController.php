<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\User;
use AppBundle\Entity\Sender;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class HomeController extends Controller
{

	 /**
     * @Route("/", name="app.homepage")
     * @Method({"GET","HEAD"})
     */
	public function indexAction(Request $request)
	{
		return $this->render('home/index.html.twig');
	}

	/**
	 * @Route("/about", name="app.about")
	 *
	 * @Method({"GET","HEAD"})
	 */
	public function aboutAction(Request $request)
	{
		return $this->render('home/about.html.twig');
	}

	/**
	 * @Route("/prices", name="app.prices")
	 * @Method({"GET","HEAD"})
	 */
	public function pricesAction(Request $request)
	{
		return $this->render('home/prices.html.twig');
	}

	/**
	 * @Route("/contact", name="app.contact")
	 */
	public function contactAction(Request $request)
	{
		return $this->render('home/contact.html.twig');
	}

}

