<?php
// src/Acme/UserBundle/Controller/RegistrationController.php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use AppBundle\Entity\User;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use AppBundle\Entity\Sender;
use AppBundle\Form\SenderType;

class RegistrationController extends BaseController
{
    public function registerAction(Request $request)
    {
        //-------------auto--------------
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        
        $user = new User();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);
        $form->handleRequest($request);
        $role = $request->query->get("role");

        $sender = new Sender();
        $senderForm = $this->createForm(SenderType::class, $sender);
        $senderForm->handleRequest($request);

        if ($role == 'sender' || $senderForm->isSubmitted() ==  true)
        {
            if ($form->isValid() && $senderForm->isValid() ) {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $em = $this->getDoctrine()->getManager();

                $em->persist($sender);
                $em->flush();
                $user->setSenderID($sender);
                $user->addRole("ROLE_SENDER");
                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
                return $response;
            }
            return $this->render('FOSUserBundle:Registration:register.html.twig', array(
                'form' => $form->createView(),
                'senderForm' => $senderForm->createView(),
            ));
        }

        elseif ($role== 'user' || $senderForm->isSubmitted() ==  false)
        {
            if ($form->isValid()  ) {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $user->addRole("ROLE_WORKER");

                $userManager->updateUser($user);
                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('fos_user_registration_confirmed');
                }
                return $this->redirectToRoute("app.worker_index");
            }

            return $this->render('FOSUserBundle:Registration:register.html.twig', array(
                'form' => $form->createView(),
                'senderForm' =>null,
            ));
        }

    }
}