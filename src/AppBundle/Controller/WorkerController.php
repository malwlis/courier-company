<?php

namespace AppBundle\Controller;

use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * User controller.
 *
 * @Route("/worker")
 * @Security("has_role('ROLE_ADMIN')")
 */

class WorkerController extends Controller
{
    /**
     * Lists all Worker entities.
     *
     * @Route("/", name="app.worker_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $role = 'ROLE_WORKER';
        $qb = $em->createQueryBuilder();
        $qb->select('u')
            ->from('AppBundle\Entity\User', 'u')
            ->where('u.roles LIKE :role')
            ->setParameter('role', "%$role%");

        $query = $qb->getQuery();
        $result = $query->getResult();

        return $this->render('user/index.html.twig', array(
            'users' => $result,
        ));
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/new", name="app.worker_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        return $this->redirect($this->generateUrl('fos_user_registration_register', array( 'role' => 'user' )));
    }


//    /**
//     * Displays a form to edit an existing User entity.
//     *
//     * @Route("/{id}/edit", name="app.worker_edit")
//     * @Method({"GET", "POST"})
//     */
//    public function editAction(Request $request, User $user)
//    {
////        $deleteForm = $this->createDeleteForm($user);
//        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
//        $editForm->handleRequest($request);
//
//        if ($editForm->isSubmitted() && $editForm->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($user);
//            $em->flush();
//
//            return $this->redirectToRoute('app.user_edit', array('id' => $user->getId()));
//        }
//
//        return $this->render('user/edit.html.twig', array(
//            'user' => $user,
//            'edit_form' => $editForm->createView(),
//        ));
//    }

//    /**
//     * Deletes a User entity.
//     *
//     * @Route("/delete/{id}", name="app.worker_delete")
//     * @Method("GET")
//     */
//    public function deleteAction(Request $request, User $worker)
//    {
//        $deleteForm = $this->createDeleteForm($worker);
//        $deleteForm->handleRequest($request);
//
//        if ($deleteForm->isSubmitted() && $deleteForm->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->remove($worker);
//            $em->flush();
//        }
//
////        return $this->redirectToRoute('app.worker_index');
//        return $this->render('user/confirm.html.twig', array(
//            'worker' => $worker,
//            'delete_form' => $deleteForm->createView(),
//        ));
//    }

    /**
     * Deletes a User entity.
     *
     * @Route("/delete/{id}", name="app.worker_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, User $worker)
    {
        $deleteForm = $this->createDeleteForm($worker);
        return $this->render('user/confirm.html.twig', array(
            'worker' => $worker,
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Deletes a Office entity.
     *
     * @Route("/delete/{id}", name="app.worker_delete_confirm")
     * @Method("DELETE")
     */
    public function deleteConfirmAction(Request $request, User $worker)
    {
        $form = $this->createDeleteForm($worker);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($worker);
            $em->flush();
        }

        return $this->redirectToRoute('app.worker_index');
    }


    /**
     * Creates a form to delete a User entity.
     *
     * @param User $user The User entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('app.worker_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
