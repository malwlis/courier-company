<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Sender
 *
 * @ORM\Table(name="sender")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SenderRepository")
 */
class Sender
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="PackageBundle\Entity\Package", mappedBy="senderID")
     */
    private $package;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255)
     */
    private $surname;


    /**
     * @ORM\ManyToOne(targetEntity="DictionaryBundle\Entity\City")
     * @ORM\JoinColumn(name="receiver_city_id", referencedColumnName="id", nullable=true)
     */
    private $city;

//    /**
//     * @ORM\ManyToOne(targetEntity="DictionaryBundle\Entity\Country")
//     * @ORM\JoinColumn(name="receiver_country_id", referencedColumnName="id", nullable=false)
//     */
//    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="homeNumber", type="string", length=50)
     */
    private $homeNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="flatNumber", type="string", length=50, nullable=true)
     */
    private $flatNumber;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Sender
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Sender
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }
    

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Sender
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Sender
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set homeNumber
     *
     * @param string $homeNumber
     *
     * @return Sender
     */
    public function setHomeNumber($homeNumber)
    {
        $this->homeNumber = $homeNumber;

        return $this;
    }

    /**
     * Get homeNumber
     *
     * @return string
     */
    public function getHomeNumber()
    {
        return $this->homeNumber;
    }

    /**
     * Set flatNumber
     *
     * @param string $flatNumber
     *
     * @return Sender
     */
    public function setFlatNumber($flatNumber)
    {
        $this->flatNumber = $flatNumber;

        return $this;
    }

    /**
     * Get flatNumber
     *
     * @return string
     */
    public function getFlatNumber()
    {
        return $this->flatNumber;
    }

    /**
     * Set package
     *
     * @param \PackageBundle\Entity\Package $package
     *
     * @return Sender
     */
    public function setPackage(\PackageBundle\Entity\Package $package = null)
    {

        $this->package->add($package);
        return $this;
//        $this->package = $package;
        
    }

    /**
     * Get package
     *
     * @return \PackageBundle\Entity\Package
     */
    public function getPackage()
    {
        return $this->package;
    }
    
    public function getAddress()
    {
        return $this->getCity()." ".$this->getStreet()." ".$this->getHomeNumber()." ".$this->getFlatNumber();
    }

    public function __toString() {
        return strval($this->getId());
    }

    /**
     * Add package
     *
     * @param \PackageBundle\Entity\Package $package
     *
     * @return Sender
     */
    public function addPackage(\PackageBundle\Entity\Package $package)
    {
        $this->package[] = $package;
        return $this;
    }

    /**
     * Remove package
     *
     * @param \PackageBundle\Entity\Package $package
     */
    public function removePackage(\PackageBundle\Entity\Package $package)
    {
        $this->package->removeElement($package);
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->package = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
