<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 */
class User extends BaseUser
{

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
    * @var int
    *
    * @ORM\OneToOne(targetEntity="Sender")
    * @ORM\JoinColumn(name="sender_id", referencedColumnName="id")
    */
    private $senderID;

    
    /**
     * Set senderID
     *
     * @param \AppBundle\Entity\Sender $senderID
     *
     * @return User
     */
    public function setSenderID(\AppBundle\Entity\Sender $senderID = null)
    {
        $this->senderID = $senderID;

        return $this;
    }

    /**
     * Get senderID
     *
     * @return \AppBundle\Entity\Sender
     */
    public function getSenderID()
    {
        return $this->senderID;
    }
}
