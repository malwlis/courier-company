<?php
/**
 * Created by PhpStorm.
 * User: Malwinka
 * Date: 2016-09-08
 * Time: 12:51
 */

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;


class Builder
{

    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Home', array('route' => ''));
//        $menu->addChild('About', array('route' => 'about'));
//        $menu->addChild('Prices', array('route' => 'prices'));
//        $menu->addChild('Contact', array('route' => 'contact'));
//
//        $menu->addChild('Follow package', array('route' => 'follow'));
//        $menu->addChild('Login', array('route' => 'login'));
//        $menu->addChild('Register', array('route' => 'register'));
//
//        $menu->addChild('Sent packages', array('route' => 'packages'));
//        $menu->addChild('Send package', array('route' => 'create'));
//
//        $menu->addChild('Logout', array('route' => 'logout'));


        return $menu;
    }
}