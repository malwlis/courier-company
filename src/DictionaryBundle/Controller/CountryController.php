<?php

namespace DictionaryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use DictionaryBundle\Entity\Country;
use DictionaryBundle\Entity\City;
use DictionaryBundle\Form\CountryType;

/**
 * Country controller.
 *
 * @Route("/country")
 */
class CountryController extends Controller
{
    /**
     * Lists all Country entities.
     *
     * @Route("/", name="dictionary.country_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $countries = $em->getRepository('DictionaryBundle:Country')->findAll();

        return $this->render('country/index.html.twig', array(
            'countries' => $countries,
        ));
    }

    /**
     * Creates a new Country entity.
     *
     * @Route("/new", name="dictionary.country_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $country = new Country();
        $form = $this->createForm('DictionaryBundle\Form\CountryType', $country);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($country);
            $em->flush();

            return $this->redirectToRoute('dictionary.country_index');
        }

        return $this->render('country/new.html.twig', array(
            'country' => $country,
            'form' => $form->createView(),
        ));
    }



    /**
     * Displays a form to edit an existing Country entity.
     *
     * @Route("/{id}/edit", name="dictionary.country_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Country $country)
    {
        $deleteForm = $this->createDeleteForm($country);
        $editForm = $this->createForm('DictionaryBundle\Form\CountryType', $country);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($country);
            $em->flush();

            return $this->redirectToRoute('dictionary.country_edit', array('id' => $country->getId()));
        }

        return $this->render('country/edit.html.twig', array(
            'country' => $country,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Deletes a Country entity.
     *
     * @Route("/delete/{id}", name="dictionary.country_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Country $country)
    {
        //sprawdzenie, czu office nie ma w localization
        $em = $this->getDoctrine()->getManager();
        $localization = $em->getRepository(City::class)->findOneBy([
            'country' => $country,
        ]);
        if ($localization != null) {
            //nie mozna - komunikat
            $countries = $em->getRepository('DictionaryBundle:Country')->findAll();

            return $this->redirectToRoute("dictionary.country_index");

        }
        else{
            $deleteForm = $this->createDeleteForm($country);

            return $this->render('country/confirm.html.twig', array(
                'country' => $country,
                'delete_form' => $deleteForm->createView(),
            ));
        }

    }

    /**
     * Deletes a Country entity.
     *
     * @Route("/delete/{id}", name="dictionary.country_delete_confirm")
     * @Method("DELETE")
     */
    public function deleteConfirmAction(Request $request, Country $country)
    {
        $form = $this->createDeleteForm($country);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($country);
            $em->flush();
        }

        return $this->redirectToRoute('dictionary.country_index');
    }

//    /**
//     * Deletes a Country entity.
//     *
//     * @Route("/{id}", name="country_delete")
//     * @Method("DELETE")
//     */
//    public function deleteAction(Request $request, Country $country)
//    {
//        $form = $this->createDeleteForm($country);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->remove($country);
//            $em->flush();
//        }
//
//        return $this->redirectToRoute('country_index');
//    }

    /**
     * Creates a form to delete a Country entity.
     *
     * @param Country $country The Country entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Country $country)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dictionary.country_delete', array('id' => $country->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
