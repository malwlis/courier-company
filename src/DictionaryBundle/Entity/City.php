<?php

namespace DictionaryBundle\Entity;

/**
 * Created by PhpStorm.
 * User: Malwinka
 * Date: 2016-12-08
 * Time: 09:40
 */

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="city")
// * @ORM\Entity(repositoryClass="DictionaryBundle\Repository\CityRepository")
 */
class City
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="DictionaryBundle\Entity\Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return City
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param \DictionaryBundle\Entity\Country $country
     *
     * @return City
     */
    public function setCountry(\DictionaryBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \DictionaryBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    public function __toString() {
        return strval($this->getCity());
    }
}
