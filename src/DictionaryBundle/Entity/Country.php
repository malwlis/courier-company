<?php

namespace DictionaryBundle\Entity;
/**
 * Created by PhpStorm.
 * User: Malwinka
 * Date: 2016-12-08
 * Time: 09:40
 */

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="country")
// * @ORM\Entity(repositoryClass="DictionaryBundle\Repository\CountryRepository")
 */
class Country
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="country", type="string", length=255)
     */
    private $country;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Country
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    public function __toString() {
        return strval($this->getCountry());
    }
}
