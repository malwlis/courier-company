<?php

namespace PackageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


class AjaxController extends Controller
{
    /**
     * @Route("/check/{param}", name="package.check")
     * @Method({"GET"})
     */
    public function check($param)
    {
        
        $data = new PackageJson($param,
            "gg","hjj","mm","kk",
            "jkl","jki", "js",
            "hjj","hjj","hj");

        $response = new JsonResponse($data);

        return $response;    
    }
    
    /**
     * @Route("/search", name="package.search")
     * @Method({"GET"})
     */
    public function searchByDateAction($dateFrom, $dateTo){
        $em = $this->getDoctrine()->getEntityManager();
        
//---------------criteria---------------------
//        $criteria = new \Doctrine\Common\Collections\Criteria();
//        $criteria->where($criteria->expr()->gt('prize', 200));
//        $result = $em->matching($criteria);
        //-------------------------------------

        $senderID = $this->getUser()->getSenderID->getId();

        $qb = $em->createQueryBuilder();
        $qb->select('p')
            ->from('AppBundle\Entity\Package', 'p')
            ->where('p.sender_id LIKE :senderID' and 'p.date_sent >= dateFrom' and 'p.date_sent <= dateTo' )
            ->setParameters(array('senderID' => "%$senderID%", 'dateFrom' => "%$dateFrom%", 'dateTo' => "%$dateTo%"));
//            ->setParameter('senderID', "%$senderID%");

        $query = $qb->getQuery();
        $packages = $query->getResult();


        $p = $packages[1];
        if ($p->getLocalizations()->isEmpty())
        {
            $localization = "Sender's home";
        }
        else{
            $localization = $p->getLocalizations()[0]->getOffice();
        }
        $packageJson = new PackageJson($localization,
            $p->getNumber(), $p->getDateSent()->format('d-m-Y H:i:s'),  $p->getStatus(), $p->getPrice(),
            $p->getReceiverName(), $p->getReceiverSurname(), $p->getReceiverCity()->getCity(),
            $p->getReceiverStreet(), $p->getReceiverHomeNumber(), $p->getReceiverFlatNumber());


        //------------------------------
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        //--------------------------------------------------

//        $packagesJson = $serializer->serialize($packages, 'json');
        $pJ = $serializer->serialize($packageJson, 'json');

        return $pJ;
            //----------------------------------------------------------
//        $response = new JsonResponse(
//            array(
//                'form' => $this->renderView('package/index.html.twig',
//                    array(
//                        "package_json"=> $packagesJson,
//                    ))), 400);
//
//        return $response;
//        ));
        
//        $response = array("code" => 100, "success" => true);
//        return new Response(json_encode($response));
        
    }
}
