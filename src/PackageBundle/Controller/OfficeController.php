<?php

namespace PackageBundle\Controller;

use PackageBundle\Entity\Localization;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use PackageBundle\Entity\Office;
use PackageBundle\Form\OfficeType;

/**
 * Office controller.
 *
 * @Route("/office")
 */
class OfficeController extends Controller
{
//    /**
//     * Lists all Office entities.
//     *
//     * @Route("/", name="office_index")
//     * @Method("GET")
//     */
//    public function indexAction()
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $offices = $em->getRepository('PackageBundle:Office')->findAll();
//
//        return $this->render('office/index.html.twig', array(
//            'offices' => $offices,
//        ));
//    }


    /**
     * @Route("/", name="package.office_get")
     * @Method({"GET"})
     */
    public function getOffice(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $offices = $em->getRepository(Office::class)->findAll();
        return $this->render('office/index.html.twig',
            array("offices" => $offices,
            ));
    }

    /**
     * @Route("/createOffice", name="package.office_create")
     * @Method({"GET","HEAD"})
     */
    public function createOffice(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $office = new Office();
        $form = $this->createForm(OfficeType::class, $office);

        return $this->render('office/create.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/createOffice", name="package.office_create_post")
     * @Method({"POST"})
     */
    public function createOfficePost(Request $request)
    {
        $office = new Office();
        $form = $this->createForm(OfficeType::class, $office);
        $form->handleRequest($request);


        if ($form->isValid()) {
            $office = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($office);
            $em->flush();

            $offices = $em->getRepository(Office::class)->findAll();
//            return $this->render('office/index.html.twig',
//                array("offices" => $offices,
//                ));
            return $this->redirectToRoute("package.office_get");
        }
    }

    /**
     * Displays a form to edit an existing Office entity.
     *
     * @Route("/edit/{id}", name="package.office_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Office $office)
    {
        $deleteForm = $this->createDeleteForm($office);
        $editForm = $this->createForm(OfficeType::class, $office);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($office);
            $em->flush();

            return $this->redirectToRoute('package.office_get');
        }
        
        return $this->render('office/edit.html.twig', array(
            'office' => $office,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Office entity.
     *
     * @Route("/delete/{id}", name="package.office_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Office $office)
    {
        //sprawdzenie, czu office nie ma w localization
        $em = $this->getDoctrine()->getManager();
        $localization = $em->getRepository(Localization::class)->findOneBy([
            'office' => $office,
        ]);
        if ($localization != null)
        {
            //nie mozna
            $offices = $em->getRepository(Office::class)->findAll();
            return $this->render('office/index.html.twig',
                array("offices" => $offices,
                    ));
        }
        
        $deleteForm = $this->createDeleteForm($office);

        return $this->render('office/confirm.html.twig', array(
            'office' => $office,
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Deletes a Office entity.
     *
     * @Route("/delete/{id}", name="package.office_delete_confirm")
     * @Method("DELETE")
     */
    public function deleteConfirmAction(Request $request, Office $office)
    {
        $form = $this->createDeleteForm($office);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($office);
            $em->flush();
        }

        return $this->redirectToRoute('package.office_get');
    }

//    /**
//     * Deletes a Office entity.
//     *
//     * @Route("/delete/{id}", name="package.office_delete")
//     * @Method("DELETE})
//     */
//    public function deleteAction(Request $request, Office $office)
//    {
//        $form = $this->createDeleteForm($office);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->remove($office);
//            $em->flush();
//        }
//
//        return $this->redirectToRoute('package.office_get');
//    }

    /**
     * Creates a form to delete a Office entity.
     *
     * @param Office $office The Office entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Office $office)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('package.office_delete', array('id' => $office->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
