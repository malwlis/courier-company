<?php
namespace PackageBundle\Controller;

use AppBundle\Entity\Sender;
use AppBundle\Form\SenderType;
use PackageBundle\Entity\Office;
use PackageBundle\Entity\Package;
use PackageBundle\Entity\Localization;
use PackageBundle\Form\LocalizationType;
use PackageBundle\Form\FollowType;
use PackageBundle\Form\PackageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


use DateTime;


class PackageController extends Controller
{


    /**
     * @Route("/get", name="package.get")
     * @Method({"GET","HEAD"})
     */
    public function viewPackagesAction(Request $request)
    {
//        $em = $this->getDoctrine()->getManager();
        $sender = $this->getUser()->getSenderID();
        $packages = $sender->getPackage();

        return $this->render('package/index.html.twig', array(
            "packages" => $packages,
                ));
        
    }
//
//    /**
//     * @Route("/get", name="package_post.get")
//     * @Method({"POST"})
//     */
//    public function viewPackagesPostAction(Request $request)
//    {
//        $this->searchByDateAction();
//    }

    /**
     * @Route("/create", name="package.create")
     * @Method({"GET","HEAD"})
     */
    public function createPackageAction(Request $request)
    {
        //$user = $this->container->get('fos_user.user_manager')->findUByUsername('admin');
        $package = new Package();
        $form = $this->createForm(PackageType::class, $package);
        $form->handleRequest($request);

        return $this->render('package/create.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/create", name="package.create_post")
     * @Method({"POST"})
     */
    public function createPackagePostAction(Request $request)
    {

        $package = new Package();
        $form = $this->createForm(PackageType::class, $package);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $package = $form->getData();

            //mail
//            $message = \Swift_Message::newInstance()
//                ->setSubject('Hello Email')
//                ->setFrom('couriernorespond@gmail.com')
//                ->setTo('mliszczyk@gmail.com')
//                ->setBody(
//                    $this->renderView(
//                        'package/createdBySender.html.twig', array(
//                            'sender' => $this->getUser()->getSenderID(),
//                            'package' => $package,)
//                    ),
//                    'text/html'
//                )
//                ;
//            $this->get('mailer')->send($message);

            //pdf
//            $this->get('knp_snappy.pdf')->generateFromHtml(
//                $this->renderView(
//                    'package/createdByWorker.html.twig', array(
//                        'sender' => $this->getUser()->getSenderID(),
//                        'package' => $package,
//                    )
//                ),
//                '/report/file.pdf'
//            );

            return $this->redirectToRoute("package.create_confirmation", array(
                'weight'=>$package->getWeight(),
                'receiverName' => $package->getReceiverName(),
                'receiverSurname' => $package->getReceiverSurname(),
                'receiverCity' => $package->getReceiverCity()->getId(),
                'receiverStreet' => $package->getReceiverStreet(),
                'receiverHomeNumber' => $package->getReceiverHomeNumber(),
                'receiverFlatNumber' => $package->getReceiverFlatNumber(),
                'senderID' => $this->getUser()->getSenderID()->getID(),
            ));
        }

        return $this->render('package/create.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/confirm/{weight}/{receiverName}/{receiverSurname}/{receiverCity}/{receiverStreet}/{receiverHomeNumber}/{receiverFlatNumber}/{senderID}", name="package.create_confirmation")
     * @Method({"GET", "HEAD"})
     */
    public function createConfirmationAction(Request $request, $weight, $receiverName, $receiverSurname, $receiverCity, $receiverStreet, $receiverHomeNumber, $receiverFlatNumber, $senderID)
    {
        $em = $this->getDoctrine()->getManager();
        $city = $em->getRepository('DictionaryBundle:City')
            ->find($receiverCity);
        $sender = $em->getRepository('AppBundle:Sender')
            ->find($senderID);

        $package = new Package();
        $package->setWeight($weight);
        $package->setReceiverCity($city);
        $package->setReceiverStreet($receiverStreet);
        $package->setReceiverHomeNumber($receiverHomeNumber);
        $package->setReceiverFlatNumber($receiverFlatNumber);
        $package->setReceiverName($receiverName);
        $package->setReceiverSurname($receiverSurname);

        $package->setPrice($package->getWeight() * 2);


        $form = $this->createForm(PackageType::class, $package);
        $form->handleRequest($request);

        return $this->render('package/createConfirmation.html.twig', array(
                'sender' => $sender,
                'package' => $package,
                'form' => $form->createView(),
            ));
    }


    /**
     * @Route("/confirm/{weight}/{receiverName}/{receiverSurname}/{receiverCity}/{receiverStreet}/{receiverHomeNumber}/{receiverFlatNumber}/{senderID}", name="package.create_confirmation_post")
     * @Method({"POST"})
     */
    public function createConfirmationPostAction(Request $request, $weight, $receiverName, $receiverSurname, $receiverCity, $receiverStreet, $receiverHomeNumber, $receiverFlatNumber, $senderID)
    {
        $em = $this->getDoctrine()->getManager();
        $city = $em->getRepository('DictionaryBundle:City')
            ->find($receiverCity);
        $sender = $em->getRepository('AppBundle:Sender')
            ->find($senderID);


        $package = new Package();
        $package->setWeight($weight);
        $package->setReceiverCity($city);
        $package->setReceiverStreet($receiverStreet);
        $package->setReceiverHomeNumber($receiverHomeNumber);
        $package->setReceiverFlatNumber($receiverFlatNumber);
        $package->setReceiverName($receiverName);
        $package->setReceiverSurname($receiverSurname);

        $package->setPrice($package->getWeight() * 2);
        $package->setSenderID($sender);
        $package->setStatus(0);
        $package->setDateSent(new \DateTime("now"));
        $package->setNumber(time().$sender->getId());

//        $em = $this->getDoctrine()->getManager();
        $em->persist($package);
        $em->flush();

        return $this->render('package/createdBySender.html.twig', array(
            'sender' => $sender,
            'package' => $package,
        ));

    }


    /**
     * @Route("/follow", name="package.follow")
     * @Method({"GET","HEAD"})
     */
    public function followPackagesAction(Request $request)
    {
        //$user = $this->container->get('fos_user.user_manager')->findUByUsername('admin');
        $package = new Package();
        $form = $this->createForm(FollowType::class, $package);
        return $this->render('package/followType.html.twig', array(
            'error' => false,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/follow", name="package.follow_post")
     * @Method("POST")
     */
    public function followPackagesPostAction(Request $request)
    {
        $package = new Package();
        $form = $this->createForm(FollowType::class, $package);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $package_nr = $form->getData()->getNumber();

            $em = $this->getDoctrine()->getManager();

            /** @var Package $package */
            $package = $em->getRepository(Package::class)->findOneBy([
                'number' => $package_nr,
            ]);


            if ($package != null)
            {
                $localization = $em->getRepository(Localization::class)->findBy([
                    'package' => $package->getId(),
                ]);

                return $this->render('package/follow.html.twig', array(
                    'package' => $package,
                    'localization' => $localization,
                ));
            }
            else{
                return $this->render('package/followType.html.twig', array(
                    'error' => true,
                    'form' => $form->createView(),
                ));
            }
        }

        //$user = $this->container->get('fos_user.user_manager')->findUByUsername('admin');
        return $this->render('package/followType.html.twig', array(
            'error' => false,
            'form' => $form->createView(),
        ));
    }
    
    
    
    
//----------------------------------------------WORKER------------------------------------------------------------------
    /**
     * @Route("/createByWorker", name="package.create_by_worker")
     * @Method({"GET","HEAD"})
     */
    public function createByWorkerAction(Request $request)
    {
        $sender = new Sender();
        $package = new Package();

        $formSender = $this->createForm(SenderType::class, $sender);
        $formPackage = $this->createForm(PackageType::class, $package);

        return $this->render('package/createWithSender.html.twig', array(
            'formSender' => $formSender->createView(),
            'formPackage' => $formPackage->createView(),
        ));

    }

    /**
     * @Route("/createByWorker", name="package.create_by_worker_post")
     * @Method({"POST"})
     */
    public function createByWorkerPostAction(Request $request)
    {
        $sender = new Sender();
        $package = new Package();
        $formSender = $this->createForm(SenderType::class, $sender);
        $formPackage = $this->createForm(PackageType::class, $package);
        $formSender->handleRequest($request);
        $formPackage->handleRequest($request);


        if ($formSender->isValid() && $formPackage->isValid()) {
            $sender = $formSender->getData();
//            $package = $formPackage->getData();
//
            $em = $this->getDoctrine()->getManager();
            $em->persist($sender);
//
//            $package->setPrice($package->getWeight() * 2);
//            $package->setStatus(0);
//            $package->setDate(new \DateTime("now"));
//            $code = time();
//            $package->setNumber($code);
//            $package->setSenderID($sender);
//
//            $em->persist($package);
//            $sender->addPackage($package);
            $em->flush();
//
//
//            return $this->render('package/createdByWorker.html.twig', array(
//                'sender' => $sender,
//                'package' => $package,
//            ));


            return $this->redirectToRoute("package.create_confirmation", array(
                'weight'=>$package->getWeight(),
                'receiverName' => $package->getReceiverName(),
                'receiverSurname' => $package->getReceiverSurname(),
                'receiverCity' => $package->getReceiverCity()->getId(),
                'receiverStreet' => $package->getReceiverStreet(),
                'receiverHomeNumber' => $package->getReceiverHomeNumber(),
                'receiverFlatNumber' => $package->getReceiverFlatNumber(),
                'senderID'=> $sender->getID,
            ));

        }

        return $this->render('package/create.html.twig', array(
            'formSender' => $formSender->createView(),
            'formPackage' => $formPackage->createView(),
        ));
    }

    /**
     * @Route("/editLocalization", name="package.edit_localization")
     * @Method({"GET", "HEAD"})
     */
    public function editLocalization(Request $request)
    {
        $localization = new Localization();
        $formLocalization = $this->createForm(LocalizationType::class, $localization);

        return $this->render('package/localization.html.twig', array(
            'form' => $formLocalization->createView(),
        ));
    }


    /**
     * @Route("/editLocalization", name="package.edit_localization_post")
     * @Method({"POST"})
     */
    public function editLocalizationPost(Request $request)
    {

        $localization = new Localization();
        $formLocalization = $this->createForm(LocalizationType::class, $localization);
        $formLocalization->handleRequest($request);


        if ($formLocalization->isValid() && $formLocalization->isValid()) {
            $data = $formLocalization->getData();
            $em = $this->getDoctrine()->getManager();

            foreach ($data->getPackage() as $package)
            {
                $l = new Localization();
                $l->setDate(new \DateTime("now"));
                $l->setPackage($package);
                $l->setOffice($data->getOffice());
                $em->persist($l);

            }
            $em->flush();
            
//            $localization->getPackage()->setlocalization($localization);
//            $em->flush();

//            return $this->render('package/createdByWorker.html.twig', array(
//                'package' => $package,
//            ));
            return $this->redirectToRoute('package.edit_localization');
        }

        return $this->render('package/localization.html.twig', array(
            'form' => $formLocalization->createView(),
        ));
    }

//    /**
//     * Creates a form to follow a Package entity.
//     *
//     * @param Package $package The Package entity
//     *
//     * @return \Symfony\Component\Form\Form The form
//     */
//    private function createFollowForm(Package $package)
//    {
//        return $this->createFormBuilder()
//            ->setAction($this->generateUrl('package.follow_post', array('id' => $package->getNumber())))
//            ->setMethod('POST')
//            ->getForm()
//            ;
//    }



    /**
     * Creates a form to confirm create a Package entity.
     *
     * @param Package $package The Package entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createConfirmForm(Office $office)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('package.office_delete', array('id' => $office->getId())))
            ->setMethod('POST')
            ->getForm()
            ;
    }
}