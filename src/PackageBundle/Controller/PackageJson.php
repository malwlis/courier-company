<?php
/**
 * Created by PhpStorm.
 * User: Malwinka
 * Date: 2017-01-21
 * Time: 20:59
 */

namespace PackageBundle\Controller;


class PackageJson{
    public function __construct($localization, $number, $dateSent,  $status, $price,
                                $receiverName, $receiverSurname, $receiverCity, $receiverStreet, $receiverHomeNumber, $receiverFlatNumber ) {
        $this->localization = $localization;
        $this->number = $number;
        $this->dateSent = $dateSent;
        $this->status = $status;
        $this->price = $price;
        $this->receiverName = $receiverName;
        $this->receiverSurname = $receiverSurname;
        $this->receiverCity = $receiverCity;
        $this->receiverStreet = $receiverStreet;
        $this->receiverHomeNumber = $receiverHomeNumber;
        $this->receiverFlatNumber = $receiverFlatNumber;
    }
    public $localization;
    public $number;
    public $dateSent;
    public $status;
    public $price;
    public $receiverName;
    public $receiverSurname;
    public $receiverCity;
    public $receiverStreet;
    public $receiverHomeNumber;
    public $receiverFlatNumber;
}