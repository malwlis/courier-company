<?php

namespace PackageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Localization
 *
 * @ORM\Table(name="localization")
 * @ORM\Entity(repositoryClass="PackageBundle\Repository\LocalizationRepository")
 */
class Localization
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *@ORM\ManyToOne(targetEntity="PackageBundle\Entity\Package", inversedBy="localizations")
     *@ORM\JoinColumn(name="package_id", referencedColumnName="id")
     */
    private $package;

    /**
     * @ORM\ManyToOne(targetEntity="PackageBundle\Entity\Office", inversedBy="localizations")
     * @ORM\JoinColumn(name="office_id", referencedColumnName="id")
     */
    private $office;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set packageId
     *
     * @param Package $package
     *
     * @return Localization
     */
    public function setPackage($package)
    {
        $this->package = $package;

        return $this;
    }

    /**
     * Get packageId
     *
     * @return Package
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * Set officeId
     *
     * @param Office $office
     *
     * @return Localization
     */
    public function setOffice($office)
    {
        $this->office = $office;

        return $this;
    }

    /**
     * Get officeId
     *
     * @return Office
     */
    public function getOffice()
    {
        return $this->office;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Localization
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }


    /**
     * Get date
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getOffice()->__toString();
    }
}
