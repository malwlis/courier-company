<?php

namespace PackageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Office
 *
 * @ORM\Table(name="office")
 * @ORM\Entity(repositoryClass="PackageBundle\Repository\OfficeRepository")
 */
class Office
{

    public function __construct() {
        $this->localizations = new ArrayCollection();
    }
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="PackageBundle\Entity\Localization", mappedBy="office")
     */
    private $localizations;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;
    // @Enum({"magazin", "główna sortownia", "EAST", "WEST"})

    /**
     * @ORM\ManyToOne(targetEntity="DictionaryBundle\Entity\City")
     * @ORM\JoinColumn(name="receiver_city_id", referencedColumnName="id", nullable=false)
     */
    private $city;

//    /**
//     * @ORM\ManyToOne(targetEntity="DictionaryBundle\Entity\Country")
//     * @ORM\JoinColumn(name="receiver_country_id", referencedColumnName="id", nullable=false)
//     */
//    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255)
     */
    private $street;

    /**
     * @var int
     *
     * @ORM\Column(name="home_number", type="string", length=50)
     */
    private $homeNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="flat_number", type="string", length=50, nullable=true)
     */
    private $flatNumber;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Office
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Office
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Office
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set homeNumber
     *
     * @param integer $homeNumber
     *
     * @return Office
     */
    public function setHomeNumber($homeNumber)
    {
        $this->homeNumber = $homeNumber;

        return $this;
    }

    /**
     * Get homeNumber
     *
     * @return int
     */
    public function getHomeNumber()
    {
        return $this->homeNumber;
    }

    /**
     * Set flatNumber
     *
     * @param integer $flatNumber
     *
     * @return Office
     */
    public function setFlatNumber($flatNumber)
    {
        $this->flatNumber = $flatNumber;

        return $this;
    }

    /**
     * Get flatNumber
     *
     * @return int
     */
    public function getFlatNumber()
    {
        return $this->flatNumber;
    }
    

    public function __toString()
    {
        return strval( $this->getCity()." ".$this->getStreet()." ".$this->getHomeNumber()." ".$this->getFlatNumber() );
    }



    /**
     * Set localization
     *
     * @param \PackageBundle\Entity\Localization $localizations
     *
     * @return Office
     */
    public function setLocalizations(\PackageBundle\Entity\Localization $localizations = null)
    {
        $this->localizations = $localizations;

        return $this;
    }

    /**
     * Get localization
     *
     * @return \PackageBundle\Entity\Localization
     */
    public function getLocalizations()
    {
        return $this->localizations;
    }

    /**
     * Add localization
     *
     * @param \PackageBundle\Entity\Localization $localization
     *
     * @return Office
     */
    public function addLocalization(\PackageBundle\Entity\Localization $localization)
    {
        $this->localizations[] = $localization;

        return $this;
    }

    /**
     * Remove localization
     *
     * @param \PackageBundle\Entity\Localization $localization
     */
    public function removeLocalization(\PackageBundle\Entity\Localization $localization)
    {
        $this->localizations->removeElement($localization);
    }


}
