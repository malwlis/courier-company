<?php

namespace PackageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Package
 *
 * @ORM\Table(name="package")
 * @ORM\Entity(repositoryClass="PackageBundle\Repository\PackageRepository")
 */
class Package
{
    public function __construct() {
        $this->localizations = new ArrayCollection();
    }
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="PackageBundle\Entity\Localization", mappedBy = "package")
     *
     */
    private $localizations;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sender", inversedBy = "package")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id")
     */
    private $senderID;
    
    /**
     * @var string
     *
     * @ORM\Column(name="weight", type="decimal", precision=2, scale=0)
     */
    private $weight;    

    /**
     * @var double
     *
     * @ORM\Column(name="number", type="bigint", unique=true)
     */
    private $number;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_sent", type="datetime", unique=false)
     */
    private $dateSent;


    /**
     * @var string
     *
     * @ORM\Column(name="receiver_name", type="string")
     */
    private $receiverName;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_surname", type="string")
     */
    private $receiverSurname;


    /**
     * @ORM\ManyToOne(targetEntity="DictionaryBundle\Entity\City")
     * @ORM\JoinColumn(name="receiver_city_id", referencedColumnName="id")
     */
    private $receiverCity;

//    /**
//     * @ORM\ManyToOne(targetEntity="DictionaryBundle\Entity\Country")
//     * @ORM\JoinColumn(name="receiver_country_id", referencedColumnName="id")
//     */
//    private $receiverCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_street", type="string")
     */
    private $receiverStreet;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_home_number", type="string", length=50)
     */
    private $receiverHomeNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver_flat_number", type="string", length=50, nullable=true)
     */
    private $receiverFlatNumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @var double
     *
     * @ORM\Column(name="price", type="decimal", precision=2, scale=0)
     */
    private $price;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set weight
     *
     * @param string $weight
     *
     * @return Package
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }


    /**
     * Set senderID
     *
     * @param integer $senderID
     *
     * @return Package
     */
    public function setSenderID($senderID)
    {
        $this->senderID = $senderID;

        return $this;
    }

    /**
     * Get senderID
     *
     * @return integer
     */
    public function getSenderID()
    {
        return $this->senderID;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Package
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return double
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set receiverCity
     *
     * @param \string $receiverCity
     *
     * @return Package
     */
    public function setReceiverCity($receiverCity)
    {
        $this->receiverCity = $receiverCity;

        return $this;
    }

    /**
     * Get receiverCity
     *
     * @return \string
     */
    public function getReceiverCity()
    {
        return $this->receiverCity;
    }

    /**
     * Set receiverStreet
     *
     * @param \string $receiverStreet
     *
     * @return Package
     */
    public function setReceiverStreet( $receiverStreet)
    {
        $this->receiverStreet = $receiverStreet;

        return $this;
    }

    /**
     * Get receiverStreet
     *
     * @return \string
     */
    public function getReceiverStreet()
    {
        return $this->receiverStreet;
    }

    /**
     * Set receiverHomeNumber
     *
     * @param integer $receiverHomeNumber
     *
     * @return Package
     */
    public function setReceiverHomeNumber($receiverHomeNumber)
    {
        $this->receiverHomeNumber = $receiverHomeNumber;

        return $this;
    }

    /**
     * Get receiverHomeNumber
     *
     * @return integer
     */
    public function getReceiverHomeNumber()
    {
        return $this->receiverHomeNumber;
    }

    /**
     * Set receiverFlatNumber
     *
     * @param integer $receiverFlatNumber
     *
     * @return Package
     */
    public function setReceiverFlatNumber($receiverFlatNumber)
    {
        $this->receiverFlatNumber = $receiverFlatNumber;

        return $this;
    }

    /**
     * Get receiverFlatNumber
     *
     * @return integer
     */
    public function getReceiverFlatNumber()
    {
        return $this->receiverFlatNumber;
    }

    /**
     * Set status
     *
     * @param \boolean $status
     *
     * @return Package
     */
    public function setStatus( $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set price
     *
     * @param  $price
     *
     * @return Package
     */
    public function setPrice( $price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return double
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set receiverName
     *
     * @param string $receiverName
     *
     * @return Package
     */
    public function setReceiverName($receiverName)
    {
        $this->receiverName = $receiverName;

        return $this;
    }

    /**
     * Get receiverName
     *
     * @return string
     */
    public function getReceiverName()
    {
        return $this->receiverName;
    }

    /**
     * Set receiverSurname
     *
     * @param string $receiverSurname
     *
     * @return Package
     */
    public function setReceiverSurname($receiverSurname)
    {
        $this->receiverSurname = $receiverSurname;

        return $this;
    }

    /**
     * Get receiverSurname
     *
     * @return string
     */
    public function getReceiverSurname()
    {
        return $this->receiverSurname;
    }

    /**
     * Set localizationId
     *
     * @param \PackageBundle\Entity\Localization $localizations
     *
     * @return Package
     */
    public function setLocalizations(\PackageBundle\Entity\Localization $localizations = null)
    {
        $this->localizations = $localizations;

        return $this;
    }

    /**
     * Get localizationId
     *
     * @return \PackageBundle\Entity\Localization
     */
    public function getLocalizations()
    {
        return $this->localizations;
    }

    public function __toString()
    {
        return strval( $this->getNumber()." To: ".$this->getReceiverName()." ".$this->getReceiverSurname().", ".$this->getReceiverCity()." ul.".$this->getReceiverStreet()." ".$this->getReceiverHomeNumber());
    }
    

    /**
     * Add localization
     *
     * @param \PackageBundle\Entity\Localization $localization
     *
     * @return Package
     */
    public function addLocalization(\PackageBundle\Entity\Localization $localization)
    {
        $this->localizations[] = $localization;

        return $this;
    }

    /**
     * Remove localization
     *
     * @param \PackageBundle\Entity\Localization $localization
     */
    public function removeLocalization(\PackageBundle\Entity\Localization $localization)
    {
        $this->localizations->removeElement($localization);
    }
    


    /**
     * Set dateSent
     *
     * @param \DateTime $dateSent
     *
     * @return Package
     */
    public function setDateSent($dateSent)
    {
        $this->dateSent = $dateSent;

        return $this;
    }

    /**
     * Get dateSent
     *
     * @return \DateTime
     */
    public function getDateSent()
    {
        return $this->dateSent;
    }


}
