<?php
/**
 * Created by PhpStorm.
 * User: Malwinka
 * Date: 2016-09-23
 * Time: 11:23
 */

namespace PackageBundle\Form;

use PackageBundle\Entity\Office;
use PackageBundle\Entity\Package;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class LocalizationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('office', EntityType::class, array(
                'class' => Office::class,
//                'choice_label' => 'city',
            ))
            ->add('package', EntityType::class, array(
                'class' => Package::class,
//                'choice_label' => ,
                'multiple' => true,
                'expanded' => true,
            ));
    }



    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PackageBundle\Entity\Localization',
        ));
    }

}