<?php
/**
 * Created by PhpStorm.
 * User: Malwinka
 * Date: 2016-09-15
 * Time: 10:14
 */


namespace PackageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class OfficeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', TextType::class)
            ->add('city', EntityType::class, array(
                'class' => 'DictionaryBundle:City',
                'choice_label' => 'city',
            ))
            ->add('street', TextType::class)
            ->add('homeNumber', TextType::class)
            ->add('flatNumber', TextType::class, array(
                'required' => false))
            ->getForm()
        ;
    }



    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PackageBundle\Entity\Office',
        ));
    }
}